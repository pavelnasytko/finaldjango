# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import Http404
from .forms import ProfileForm, PasswordChangeForm

def profile_view(request):
    if not request.user.is_authenticated():
        raise Http404

    email_exists = False
    if request.method == "POST":
        if 'password_change_button' in request.POST:
            user = request.user
            form_pass = PasswordChangeForm(request.POST)
            form = ProfileForm({'firstname': user.first_name, 'lastname': user.last_name, 'email': user.email})
            if form_pass.is_valid():
                password = form_pass.cleaned_data['password']
                user = request.user
                user.set_password(password)
                user.save()
        else:
            form_pass = PasswordChangeForm()
            form = ProfileForm(request.POST)
            if form.is_valid():
                firstname = form.cleaned_data['firstname']
                lastname = form.cleaned_data['lastname']
                email = form.cleaned_data['email']
                user = request.user
                if user.email != email and email in User.objects.all().values_list('email', flat=True):
                    email_exists = True
                else:
                    user.first_name = firstname
                    user.last_name = lastname
                    user.email = email
                    user.save()
    else:
        user = request.user
        form = ProfileForm({'firstname': user.first_name, 'lastname': user.last_name, 'email': user.email})
        form_pass = PasswordChangeForm()

    return render(request, 'profuzyt/profile.html', {'form': form, 'form_pass': form_pass,
        'email_exists': email_exists})
