from django.test import TestCase, Client
from django.contrib.auth.models import User

class ProfileViewTests(TestCase):
    def test_email_exists(self):
        c = Client()
        User.objects.create_user(
            username='adam', password='adampass', email='mail@box.com')
        User.objects.create_user(
            username='adams', password='adampass', email='mail2@box.com')
        c.login(username='adams', password='adampass')
        response = c.post('/profile/', {'firstname': 'Adam', 'lastname': 'Adamss',
            'email': 'mail@box.com'})
        self.assertTrue(response.context['email_exists'])

    def test_user_not_logged_in(self):
        c = Client()
        response = c.post('/profile/', {'firstname': 'Adam', 'lastname': 'Adamss',
            'email': 'mail@box.com'}, follow=True)
        self.assertEqual(response.status_code, 404)