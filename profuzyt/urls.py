from django.conf.urls import url

from . import views

app_name = 'profuzyt'
urlpatterns = [
    url(r'^$', views.profile_view, name='profile'),
]