# ~*~ coding: utf-8 ~*~

from django.conf.urls import url

from . import views

app_name = 'blog'
urlpatterns = [
    url(r'^$', views.main_page, name='main'),
    #url(r'^$', main_page),
    # Если передан id поста, то отдаем управление get_post вьюхе
   url(r'^post/([0-9]{1,5})', views.get_post,name='post'),
    #url(r'^post/([0-9]{1,5})', views.poll_summary, name='summary'),
    
]