# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .forms import LoginForm, RegisterForm, RegisterResendForm
from .models import UserConfirmation
from django.template import RequestContext
import random, string
from .e_sen import send_confirmation_email

def login_view(request):
    user_not_exists = False
    not_confirmed = False
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if not user:
                user_not_exists = True
            else:
                userconf = UserConfirmation.objects.filter(user=user).first()
                if userconf is None:
                    login(request, user)
                    return HttpResponseRedirect(reverse('home'))
                if userconf.is_confirmed:
                    login(request, user)
                    return HttpResponseRedirect(reverse('home'))
                else:
                    not_confirmed = True
    else:
        form = LoginForm()

    return render(request, 'loginsys/login.html', {'form': form, 'user_not_exists': user_not_exists,
        'not_confirmed': not_confirmed})

def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))

def register_view(request):
    username_exists = False
    email_exists = False
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            firstname = form.cleaned_data['firstname']
            lastname = form.cleaned_data['lastname']
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            if username in User.objects.all().values_list('username', flat=True):
                username_exists = True
            elif email in User.objects.all().values_list('email', flat=True):
                email_exists = True
            else:
                user = User.objects.create_user(username=username, email=email, password=password, 
                    first_name=firstname, last_name=lastname)
                random_code = ''.join(random.choices(string.ascii_uppercase + string.digits, k=16))
                UserConfirmation.objects.create(user=user, is_confirmed=False, confirm_code=random_code)
                send_confirmation_email(user.email, user.username, random_code)
                context = {
                    'username': user.username,
                    'firstname': user.first_name,
                    'lastname': user.last_name,
                    'email': user.email,
                }
                return render(request, 'loginsys/register_complete.html', context)
    else:
        form = RegisterForm()

    return render(request, 'loginsys/register.html', {'form': form, 
        'username_exists': username_exists, 'email_exists': email_exists})

def register_resend_view(request):
    email_exists = True
    user_confirmed = False
    was_post = False
    if request.method == "POST":
        form = RegisterResendForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            if email not in User.objects.all().values_list('email', flat=True):
                email_exists = False
            else:
                user = get_object_or_404(User, email=email)
                userconf = get_object_or_404(UserConfirmation, user=user)
                if UserConfirmation.objects.get(user=user).is_confirmed:
                    user_confirmed = True
                else:
                    was_post = True
                    send_confirmation_email(user.email, user.username, userconf.confirm_code)
                    return render(request, 'loginsys/register_resend.html', {'email_exists': email_exists, 
                        'was_post': was_post, 'user_confirmed': user_confirmed, 'email': email})
    else:
        form = RegisterResendForm()

    return render(request, 'loginsys/register_resend.html', {'form': form, 'email_exists': email_exists, 
        'was_post': was_post, 'user_confirmed': user_confirmed})
    
def register_confirmation_view(request, username, register_code):
    user = get_object_or_404(User, username=username)
    userconf = get_object_or_404(UserConfirmation, user=user)
    confirmed = False
    print("DANY KOD: {0} KOD UZYTKOWNIKA {1}".format(userconf.confirm_code, register_code))
    if userconf.is_confirmed:
        confirmed = True
    elif userconf.confirm_code != register_code:
        raise Http404
    else:
        userconf.is_confirmed = True
        userconf.save()
        login(request, user)

    return render(request, 'loginsys/register_confirm.html', {'confirmed': confirmed})