# -*- coding: utf-8 -*-

from django import forms
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from captcha.fields import CaptchaField
import re

class LoginForm(forms.Form):
    username = forms.CharField(label='Nazwa użytkownika', required=False, max_length=30)
    password = forms.CharField(widget=forms.PasswordInput, label='Hasło', required=False, max_length=30)

    def clean_username(self):
        data = self.cleaned_data['username']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        return data

    def clean_password(self):
        data = self.cleaned_data['password']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        return data

class RegisterForm(forms.Form):
    firstname = forms.CharField(label='Imię', required=False, max_length=30)
    lastname = forms.CharField(label='Nazwisko', required=False, max_length=30)
    username = forms.CharField(label='Nazwa użytkownika', required=False, max_length=30)
    password = forms.CharField(widget=forms.PasswordInput, label='Hasło', required=False, max_length=30)
    repeatpassword = forms.CharField(widget=forms.PasswordInput, label='Powtórz hasło', required=False, max_length=30)
    email = forms.CharField(label='Adres e-mail', required=False)
    captcha = CaptchaField(label='Kod CAPTCHA')

    firstname_regex = re.compile("^[ĘÓĄŚŁŻŹĆŃA-Z][ęóąśłżźćńa-z]+$")
    lastname_regex = re.compile("^[ĘÓĄŚŁŻŹĆŃA-Z][ęóąśłżźćńa-z]+$")
    username_regex = re.compile("^[a-z0-9]+$")
    min_username_length = 6
    password_regex = re.compile("^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@.\+\-_])[A-Za-z\d@.+-_]+$")
    min_password_length = 6

    def clean_firstname(self):
        data = self.cleaned_data['firstname']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if not self.firstname_regex.match(data):
            raise forms.ValidationError(
                "Niepoprawny format imienia."
            )
        return data

    def clean_lastname(self):
        data = self.cleaned_data['lastname']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if not self.lastname_regex.match(data):
            raise forms.ValidationError(
                "Niepoprawny format nazwiska."
            )
        return data

    def clean_username(self):
        data = self.cleaned_data['username']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if not self.username_regex.match(data):
            raise forms.ValidationError(
                "Nazwa użytkownika może składać się tylko z małych liter i cyfer."
            )
        if len(data) < self.min_username_length:
            raise forms.ValidationError(
                "Długośc nazwy użytkownika musi mieć minimum 6 znaków."
            )
        return data

    def clean_password(self):
        data = self.cleaned_data['password']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if not self.password_regex.match(data):
            raise forms.ValidationError(
                "Hasło może się składać tylko z liter, dużych liter, cyfr i @/./+/-/_ " + 
                "i musi mieć po jednym z tych znaków."
            )
        if len(data) < self.min_password_length:
            raise forms.ValidationError(
                "Długośc hasła musi mieć mieć minimum 6 znaków."
            )
        return data

    def clean_repeatpassword(self):
        data = self.cleaned_data['repeatpassword']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if 'password' in self.cleaned_data and data != self.cleaned_data['password']:
            raise forms.ValidationError(
                "Podane hasła muszą do siebie pasować."
            )
        return data

    def clean_email(self):
        data = self.cleaned_data['email']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        try:
            validate_email(data)
        except ValidationError as e:
            raise forms.ValidationError(
                "Nie poprawny adres e-mail."
            )
        return data

    def clean_captcha(self):
        data = self.cleaned_data['captcha']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        return data

class RegisterResendForm(forms.Form):
    email = forms.CharField(label='Adres e-mail', required=False)

    def clean_email(self):
        data = self.cleaned_data['email']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        try:
            validate_email(data)
        except ValidationError as e:
            raise forms.ValidationError(
                "Nie poprawny adres e-mail."
            )
        return data