# -*- coding: utf-8 -*-

from django.test import TestCase, Client
from .models import UserConfirmation
from django.contrib.auth.models import User

class LoginViewTests(TestCase):
    def test_user_does_not_exists(self):
        c = Client()
        response = c.post('/auth/login/', {'username': 'john', 'password': 'smith'})
        self.assertTrue(response.context['user_not_exists'])

    def test_user_exists_and_logged_in(self):
        c = Client()
        User.objects.create_user(username='adam', password='adampass')
        response = c.post('/auth/login/', {'username': 'adam', 'password': 'adampass'}, follow=True)
        self.assertEqual(len(response.redirect_chain), 1)
        self.assertTrue(response.context['user'].is_authenticated())

    def test_user_not_confirmed(self):
        c = Client()
        u = User.objects.create_user(username='adam', password='adampass')
        UserConfirmation.objects.create(user=u, is_confirmed=False, confirm_code='ABC')
        response = c.post('/auth/login/', {'username': 'adam', 'password': 'adampass'})
        self.assertTrue(response.context['not_confirmed'])

    def test_user_confirmed(self):
        c = Client()
        u = User.objects.create_user(username='adam', password='adampass')
        UserConfirmation.objects.create(user=u, is_confirmed=True, confirm_code='ABC')
        response = c.post('/auth/login/', {'username': 'adam', 'password': 'adampass'}, follow=True)
        self.assertEqual(len(response.redirect_chain), 1)

class LogoutViewTests(TestCase):
    def test_logout(self):
        c = Client()
        response = c.get('/auth/logout/', follow=True)
        self.assertEqual(len(response.redirect_chain), 1)
        self.assertFalse(response.context['user'].is_authenticated())

class RegisterViewTests(TestCase):
    def test_wrong_captcha(self):
        c = Client()
        response = c.post('/auth/register/', {'firstname': 'Adam', 'lastname': 'Adamss',
            'username': 'adammm', 'password': 'Adampass2_', 'email': 'mail@box.com'})
        self.assertFalse(response.context['form'].is_valid())
        