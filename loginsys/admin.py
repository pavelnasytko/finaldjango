from django.contrib import admin

from .models import UserConfirmation

admin.site.register(UserConfirmation)
