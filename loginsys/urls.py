from django.conf.urls import url

from . import views

app_name = 'loginsys'
urlpatterns = [
    url(r'^login/$', views.login_view, name='login'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^register/$', views.register_view, name='register'),
    url(r'^register/(?P<username>[a-z0-9]+)/(?P<register_code>[0-9A-Z]{16,})/$', 
        views.register_confirmation_view, name='register_confirm'),
    url(r'^register/resend', views.register_resend_view, name='register_resend'),
]