from django.db import models
from django.contrib.auth.models import User

class UserConfirmation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_confirmed = models.BooleanField()
    confirm_code = models.CharField(max_length=16)
    def __str__(self):
        if self.is_confirmed:
            return "User:{" + self.user.username + "} confirmed"
        else:
            return "User:{" + self.user.username + "} not confirmed"