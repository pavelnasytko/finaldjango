import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_confirmation_email(receiver, username, confirmation_code):
    SITE_ADDRESS = '127.0.0.1'
    EMAIL_ADDRESS = 'pbnasytko@gmail.com'
    EMAIL_PASSWORD = 'paveltest123'
    EMAIL_SMTP = 'smtp.gmail.com'
    EMAIL_SMTP_PORT = 587
    EMAIL_SUBJECT = 'Potwierdzenie rejestracji - E-Wybory'
    EMAIL_MESSAGE = """\
    <html>
      <head></head>
      <body>
        Aktywowac: 
        <a href="http://""" + SITE_ADDRESS + """/auth/register/{0}/{1}">kliknij tutaj</a>.
      </body>
    </html>
    """

    msg = MIMEMultipart('alternative')
    msg['From'] = EMAIL_ADDRESS
    msg['Subject'] = EMAIL_SUBJECT
    msg['To'] = receiver
    
    html = EMAIL_MESSAGE
    msg.attach(MIMEText(html.format(username, confirmation_code), 'html'))
    
    sender = EMAIL_ADDRESS
    passwd = EMAIL_PASSWORD
    server = smtplib.SMTP(EMAIL_SMTP, EMAIL_SMTP_PORT)
    server.ehlo()
    server.starttls()
    server.login(sender, passwd)
    try:
        server.sendmail(sender, receiver, msg.as_string())
    except:
        print('Error sending mail')
    server.quit()