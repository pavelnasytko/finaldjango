"""SWybor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.conf.urls import (
handler400, handler403, handler404, handler500
)

# handler400 = 'views.bad_request'
# handler403 = 'views.permission_denied'
# handler404 = 'views.page_not_found'
# handler500 = 'views.server_error'

urlpatterns = [
    url(r'^$', views.home_view, name='home'),
    url(r'^admin/', admin.site.urls),
    url(r'^polls/', include('polls.urls')),
    url(r'^auth/', include('loginsys.urls')),
    url(r'^profile/', include('profuzyt.urls')),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^maps/', include('maps.urls')),
    url(r'^news/', include('blog.urls')),
   
]
