# -*- coding: utf-8 -*-

from datetime import timedelta

def to_ascii(str):
    tmp = []
    for i in range(0,len(str)):
        if str[i] == 'ę':
            tmp.append('e')
        elif str[i] == 'ó':
            tmp.append('o')
        elif str[i] == 'ą':
            tmp.append('a')
        elif str[i] == 'ś':
            tmp.append('s')
        elif str[i] == 'ł':
            tmp.append('l')
        elif str[i] == 'ż':
            tmp.append('z')
        elif str[i] == 'ź':
            tmp.append('z')
        elif str[i] == 'ń':
            tmp.append('n')
        elif str[i] == 'Ę':
            tmp.append('E')
        elif str[i] == 'Ó':
            tmp.append('O')
        elif str[i] == 'Ą':
            tmp.append('A')
        elif str[i] == 'Ś':
            tmp.append('S')
        elif str[i] == 'Ł':
            tmp.append('L')
        elif str[i] == 'Ż':
            tmp.append('Z')
        elif str[i] == 'Ź':
            tmp.append('Z')
        elif str[i] == 'Ń':
            tmp.append('N')
        else:
            tmp.append(str[i])
    return "".join(tmp)