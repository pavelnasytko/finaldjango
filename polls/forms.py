# -*- coding: utf-8 -*-

from django import forms
from django.utils import timezone
import re

class PollCreateForm(forms.Form):
    name = forms.CharField(label='Nazwa ankiety', required=False, max_length=50)
    pollstart = forms.DateTimeField(label='Data rozpoczęcia ankiety', 
        required=False, input_formats=['%d/%m/%Y %H:%M:%S'], 
        error_messages={'invalid': 'Nie poprawna data/czas'})
    pollend = forms.DateTimeField(label='Data zakończenia ankiety', 
        required=False, input_formats=['%d/%m/%Y %H:%M:%S'], 
        error_messages={'invalid': 'Nie poprawna data/czas'})
    maxvote = forms.CharField(widget=forms.NumberInput, label='Maksymalna ilość wyborów', 
        required=False, max_length=5) 

    name_regex = re.compile("^[ĘÓĄŚŁŻŹĆŃA-Z][ęóąśłżźćńa-z ]+$")

    def clean_name(self):
        data = self.cleaned_data['name']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if not self.name_regex.match(data):
            raise forms.ValidationError(
                "Nazwa ankiety musi zaczynać się dużą literą i kończyć małymi literami."
            )
        return data

    def clean_pollstart(self):
        data = self.cleaned_data['pollstart']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if data < timezone.now():
            raise forms.ValidationError(
                "Czas rozpoczęcia ankiety nie może być wcześniejszy niż aktualny czas."
            )
        return data

    def clean_pollend(self):
        data = self.cleaned_data['pollend']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if 'pollstart' in self.cleaned_data and data <= self.cleaned_data['pollstart']:
            raise forms.ValidationError(
                "Czas zakończenia ankiety nie może być wcześniejszy lub równy rozpoczęciu ankiety."
            )
        return data

    def clean_maxvote(self):
        data = self.cleaned_data['maxvote']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if int(data) <= 0:
            raise forms.ValidationError(
                "Ilość głosów nie może być mniejsza lub równa 0."
            ) 
        return data

class PollChangeForm(forms.Form):
    name = forms.CharField(label='Nazwa ankiety', required=False, max_length=50)
    pollstart = forms.DateTimeField(label='Data rozpoczęcia ankiety', 
        required=False, input_formats=['%d/%m/%Y %H:%M:%S'], 
        error_messages={'invalid': 'Nie poprawna data/czas'})
    pollend = forms.DateTimeField(label='Data zakończenia ankiety', 
        required=False, input_formats=['%d/%m/%Y %H:%M:%S'], 
        error_messages={'invalid': 'Nie poprawna data/czas'})
    maxvote = forms.CharField(widget=forms.NumberInput, label='Maksymalna ilość wyborów', 
        required=False, max_length=5) 

    name_regex = re.compile("^[ĘÓĄŚŁŻŹĆŃA-Z][ęóąśłżźćńa-z ]+$")

    def clean_name(self):
        data = self.cleaned_data['name']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if not self.name_regex.match(data):
            raise forms.ValidationError(
                "Nazwa ankiety musi zaczynać się dużą literą i kończyć małymi literami."
            )
        return data

    def clean_pollstart(self):
        data = self.cleaned_data['pollstart']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        return data

    def clean_pollend(self):
        data = self.cleaned_data['pollend']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if 'pollstart' in self.cleaned_data and data <= self.cleaned_data['pollstart']:
            raise forms.ValidationError(
                "Czas zakończenia ankiety nie może być wcześniejszy lub równy rozpoczęciu ankiety."
            )
        return data

    def clean_maxvote(self):
        data = self.cleaned_data['maxvote']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if int(data) <= 0:
            raise forms.ValidationError(
                "Ilość głosów nie może być mniejsza lub równa 0."
            ) 
        return data

class PollUsersChange(forms.Form):
    maxvote = forms.CharField(widget=forms.NumberInput, label='Maksymalna ilość wyborów', 
        required=False, max_length=5)

    def clean_maxvote(self):
        data = self.cleaned_data['maxvote']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if int(data) <= 0:
            raise forms.ValidationError(
                "Ilość głosów nie może być mniejsza lub równa 0."
            ) 
        return data

class StartedPollChange(forms.Form):
    name = forms.CharField(label='Nazwa ankiety', required=False, max_length=50)
    pollstart = forms.DateTimeField(initial='DD/MM/RRRR GG:MM:SS', label='Data rozpoczęcia ankiety', 
        required=False, input_formats=['%d/%m/%Y %H:%M:%S'], 
        error_messages={'invalid': 'Nie poprawna data/czas'})
    pollend = forms.DateTimeField(initial='DD/MM/RRRR GG:MM:SS', label='Data zakończenia ankiety', 
        required=False, input_formats=['%d/%m/%Y %H:%M:%S'], 
        error_messages={'invalid': 'Nie poprawna data/czas'})

    name_regex = re.compile("^[ĘÓĄŚŁŻŹĆŃA-Z][ęóąśłżźćńa-z ]+$")

    def clean_name(self):
        data = self.cleaned_data['name']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if not self.name_regex.match(data):
            raise forms.ValidationError(
                "Nazwa ankiety musi zaczynać się dużą literą i kończyć małymi literami."
            )
        return data

    def clean_pollstart(self):
        data = self.cleaned_data['pollstart']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        return data

    def clean_pollend(self):
        data = self.cleaned_data['pollend']
        if not data:
            raise forms.ValidationError(
                "Te pole musi zostać uzupełnione."
            )
        if 'pollstart' in self.cleaned_data and data <= self.cleaned_data['pollstart']:
            raise forms.ValidationError(
                "Czas zakończenia ankiety nie może być wcześniejszy lub równy rozpoczęciu ankiety."
            )
        return data