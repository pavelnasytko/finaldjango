from django.conf.urls import url

from . import views

app_name = 'polls'
urlpatterns = [
    url(r'^create/$', views.create_poll_view, name='create'),
    url(r'^active/$', views.ActivePollView.as_view(), name='active'),
    url(r'^ended/$', views.EndedPollView.as_view(), name='ended'),
    url(r'^incoming/$', views.IncomingPollView.as_view(), name='incoming'),
    url(r'^(?P<poll_id>[1-9][0-9]*)/$', views.poll_option_view, name='option'),
    url(r'^(?P<poll_id>[1-9][0-9]*)/summary/$', views.poll_summary, name='summary'),
    url(r'^(?P<poll_id>[1-9][0-9]*)/vote/$', views.vote_view, name='vote'),
    url(r'^(?P<poll_id>[1-9][0-9]*)/vote/make/$', views.vote, name='votemake'),
    url(r'^mypolls/$', views.MyPollsView.as_view(), name='mypolls'),
    url(r'^(?P<poll_id>[1-9][0-9]*)/change/$', views.change_poll_view, name='change'),
]