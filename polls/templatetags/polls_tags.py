# -*- coding: utf-8 -*-
from django import template
from django.utils import timezone
from polls.models import Poll, PollOption
from django.contrib.auth.models import User

register = template.Library()

@register.assignment_tag
def poll_attendance(poll_id):
    poll = Poll.objects.get(pk=poll_id)
    user_votes = {}
    for authorized in poll.authorized_set.all():
        user_votes[authorized.user] = False
    votes_quantity = 0
    for authorized in poll.authorized_set.all():
        for option in poll.polloption_set.all():
            if option.vote_set.filter(voter=authorized.user).count() > 0:
                user_votes[authorized.user] = True
    authorized_quantity = poll.authorized_set.all().count()
    if authorized_quantity == 0:
        return 0
    for authorized, key in user_votes.items():
        if key == True:
            votes_quantity += 1
    return votes_quantity / authorized_quantity * 100

@register.assignment_tag
def can_user_vote(username, poll_id):
    poll = Poll.objects.get(pk=poll_id)
    for option in poll.polloption_set.all():
        for vote in option.vote_set.all():
            if vote.voter.username == username:
                return 1
    for authorized in poll.authorized_set.all():
        if authorized.user.username == username:
            return 2
    return 0

@register.assignment_tag
def poll_status(poll_id):
    poll = Poll.objects.get(pk=poll_id)
    if poll.vote_end <= timezone.now():
        return 0
    if poll.vote_start > timezone.now():
        return 1
    return 2

@register.assignment_tag
def poll_winner(poll_id):
    poll = Poll.objects.get(pk=poll_id)
    data = {}
    max = 0
    for key in PollOption.objects.filter(poll=poll):
        data[key.candidate] = key.vote_set.count()
        if max < data[key.candidate]:
            max = data[key.candidate]
    if max == 0:
        return []
    data_return = []
    for key, value in data.items():
        if max == value:
            data_return.append(key)
    return data_return