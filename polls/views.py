# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.utils.timezone import localtime
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from django.views import generic
from django.contrib.auth.models import User
from .models import *
from .forms import PollCreateForm, PollChangeForm, StartedPollChange, PollUsersChange
from .fusioncharts import FusionCharts
from .custom_functions import to_ascii

class ActivePollView(generic.ListView):
    context_object_name = 'active_polls_list'
    template_name = 'polls/active.html'
    def get_queryset(self):
        return Poll.objects.filter(vote_start__lte=timezone.now(),vote_end__gte=timezone.now())

class EndedPollView(generic.ListView):
    context_object_name = 'ended_polls_list'
    template_name = 'polls/ended.html'
    def get_queryset(self):
        return Poll.objects.filter(vote_end__lt=timezone.now())

class IncomingPollView(generic.ListView):
    context_object_name = 'incoming_polls_list'
    template_name = 'polls/incoming.html'
    def get_queryset(self):
        return Poll.objects.filter(vote_start__gt=timezone.now())

def vote_view(request, poll_id):
    poll = get_object_or_404(Poll, pk=poll_id)
    vote_timeout = False
    too_many_options = False
    no_options = False
    if timezone.now() < poll.vote_start:
        if poll.creator == request.user:
            return change_poll_view(request, poll_id)
        raise Http404
    if timezone.now() > poll.vote_end:
        vote_timeout = True
    return render(request, 'polls/vote.html', {'vote_timeout': vote_timeout, 
        'too_many_options': too_many_options, 'no_options': no_options, 'poll': poll})

def vote(request, poll_id):
    vote_timeout = False
    too_many_options = False
    no_options = False
    poll = get_object_or_404(Poll, pk=poll_id)
    if timezone.now() < poll.vote_start:
        raise Http404
    user_options = request.POST.getlist('option')
    if timezone.now() > poll.vote_end:
        vote_timeout = True
    if len(user_options) > poll.max_vote_number:
        too_many_options = True
    if len(user_options) == 0:
        no_options = True
    if vote_timeout == True or too_many_options == True or no_options == True:
        return render(request, 'polls/vote.html', {'vote_timeout': vote_timeout, 
            'too_many_options': too_many_options, 'no_options': no_options, 'poll': poll})
    for user in user_options:
        polloption = poll.polloption_set.get(candidate=User.objects.get_by_natural_key(username=user))
        Vote.objects.create(voter=request.user, option=polloption)
    return render(request, 'polls/option.html', {'poll': poll})

class MyPollsView(generic.ListView):
    context_object_name = 'my_polls_list'
    template_name = 'polls/mypolls.html'
    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated():
            return Poll.objects.filter(creator=user)
        else:
            raise Http404

def poll_option_view(request, poll_id):
    poll = get_object_or_404(Poll, pk=poll_id)

    if poll.vote_end <= timezone.now():
        return HttpResponseRedirect(reverse('polls:summary', kwargs={'poll_id': poll.id}))

    return render(request, 'polls/option.html', {'poll': poll})

def create_poll_view(request):
    if not request.user.is_authenticated():
        raise Http404
    maxvote_lower = False
    candidates_empty = True
    authorized_empty = True
    if request.method == "POST":
        form = PollCreateForm(request.POST)
        candidates = {}
        authorized = {}
        candidates_count = 0
        for user in User.objects.all():
            check_candidate = request.POST.getlist('{0}candidate'.format(user.username))
            check_authorized = request.POST.getlist('{0}authorized'.format(user.username))
            if check_candidate:
                candidates[user] = 'checked'
                candidates_count += 1
                candidates_empty = False
            else:
                candidates[user] = ''
            if check_authorized:
                authorized[user] = 'checked'
                authorized_empty = False
            else:
                authorized[user] = ''
        if form.is_valid():
            name = form.cleaned_data['name']
            pollstart = form.cleaned_data['pollstart']
            pollend = form.cleaned_data['pollend']
            maxvote = form.cleaned_data['maxvote']
            if candidates_count < int(maxvote):
                maxvote_lower = True
            elif candidates_empty == False and authorized_empty == False:
                created_poll = Poll.objects.create(name=name, creator=request.user, 
                    vote_start=pollstart, vote_end=pollend, max_vote_number=maxvote)
                for user, check in candidates.items():
                    if check == 'checked':
                        PollOption.objects.create(poll=created_poll, candidate=user)
                for user, check in authorized.items():
                    if check == 'checked':
                        Authorized.objects.create(poll=created_poll, user=user)
                return render(request, 'polls/create_summary.html', {'poll': created_poll})
    else:
        form = PollCreateForm()
        candidates = {}
        authorized = {}
        candidates_empty = False
        authorized_empty = False
        for user in User.objects.all():
            candidates[user] = ''
            authorized[user] = ''

    return render(request, 'polls/create.html', {'form': form, 'maxvote_lower': maxvote_lower,
        'candidates': candidates, 'authorized': authorized, 'candidates_empty': candidates_empty, 
        'authorized_empty': authorized_empty})

def change_poll_view(request, poll_id):
    maxvote_lower = False
    candidates_empty = True
    authorized_empty = True
    vote_started = False
    if request.method == "POST":
        poll = get_object_or_404(Poll, pk=poll_id)
        if timezone.now() > poll.vote_start and timezone.now() < poll.vote_end:
            vote_started = True
        if request.POST.get("update_users_button"):
            candidates = {}
            authorized = {}
            form = PollUsersChange(request.POST)
            candidates_count = 0
            for user in User.objects.all():
                check_candidate = request.POST.getlist('{0}candidate'.format(user.username))
                check_authorized = request.POST.getlist('{0}authorized'.format(user.username))
                if check_candidate:
                    candidates[user] = 'checked'
                    candidates_count += 1
                    candidates_empty = False
                else:
                    candidates[user] = ''
                if check_authorized:
                    authorized[user] = 'checked'
                    authorized_empty = False
                else:
                    authorized[user] = ''
            if form.is_valid():
                maxvote = form.cleaned_data['maxvote']
                if candidates_count < int(maxvote):
                    maxvote_lower = True
                elif candidates_empty == False and authorized_empty == False:
                    poll.max_vote_number = maxvote
                    PollOption.objects.filter(poll=poll).delete()
                    Authorized.objects.filter(poll=poll).delete()
                    for user, check in candidates.items():
                        if check == 'checked':
                            PollOption.objects.create(poll=poll, candidate=user)
                    for user, check in authorized.items():
                        if check == 'checked':
                            Authorized.objects.create(poll=poll, user=user)
                    poll.save()
                return HttpResponseRedirect(reverse('polls:mypolls'))
        elif request.POST.get("update_else_button"):
            form = StartedPollChange(request.POST)
            candidates = {}
            authorized = {}
            if form.is_valid():
                name = form.cleaned_data['name']
                pollstart = form.cleaned_data['pollstart']
                pollend = form.cleaned_data['pollend']
                poll.name = name
                poll.vote_start = pollstart
                poll.vote_end = pollend
                poll.save()
                return HttpResponseRedirect(reverse('polls:mypolls'))
        else:
            if not vote_started:
                poll.delete()
                return HttpResponseRedirect(reverse('polls:mypolls'))
    else:
        poll = get_object_or_404(Poll, pk=poll_id)
        if poll.creator != request.user:
            raise Http404
        if timezone.now() > poll.vote_start and timezone.now() < poll.vote_end:
            vote_started = True
        if vote_started:
            candidates = {}
            authorized = {}
            candidates_empty = False
            authorized_empty = False
            vote_start = localtime(poll.vote_start).strftime("%d/%m/%Y %H:%M:%S")
            vote_end = localtime(poll.vote_end).strftime("%d/%m/%Y %H:%M:%S")
            form = StartedPollChange({'name': poll.name, 'pollstart': vote_start, 'pollend': vote_end,
                'maxvote': poll.max_vote_number})
        else:
            vote_start = localtime(poll.vote_start).strftime("%d/%m/%Y %H:%M:%S")
            vote_end = localtime(poll.vote_end).strftime("%d/%m/%Y %H:%M:%S")
            form = PollChangeForm({'name': poll.name, 'pollstart': vote_start, 'pollend': vote_end,
                'maxvote': poll.max_vote_number})
            candidates = {}
            authorized = {}
            candidates_empty = False
            authorized_empty = False
            for option in PollOption.objects.filter(poll=poll):
                candidates[option.candidate] = 'checked'
            for auth in Authorized.objects.filter(poll=poll):
                authorized[auth.user] = 'checked'
            for user in User.objects.all():
                if not user in candidates:
                    candidates[user] = ''
                if not user in authorized:
                    authorized[user] = ''

    return render(request, 'polls/change.html', {'form': form, 'maxvote_lower': maxvote_lower,
        'candidates': candidates, 'authorized': authorized, 'candidates_empty': candidates_empty, 
        'authorized_empty': authorized_empty, 'poll_id': poll_id, 'vote_started': vote_started})


def poll_summary(request, poll_id):
    poll = get_object_or_404(Poll, pk=poll_id)
    if timezone.now() < poll.vote_end:
        raise Http404
    dataSource = {}
    dataSource['chart'] = {
            "caption": to_ascii("Wyniki glosowania na ankiete '{0}'".format(poll.name)),
            "xAxisName": "Kandydat",
            "yAxisName": "Ilosc glosow",
            "theme": "zune"
    }
      
    dataSource['data'] = []

    for key in PollOption.objects.filter(poll=poll):
        data = {}
        data['label'] = to_ascii(key.candidate.first_name + " " + key.candidate.last_name)
        data['value'] = key.vote_set.count()
        dataSource['data'].append(data)

    column2D = FusionCharts("column2D", "ex1" , "820", "500", "chart-1", "json", dataSource)
    return render(request, 'polls/summary.html', {'output': column2D.render(),
        'poll': poll})
