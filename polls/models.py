from django.db import models
from django.contrib.auth.models import User

class Poll(models.Model):
    name = models.CharField(max_length=50)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    vote_start = models.DateTimeField()
    vote_end = models.DateTimeField()
    max_vote_number = models.DecimalField(decimal_places=0, max_digits=5)
    def __str__(self):
        return self.name

class PollOption(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    candidate = models.ForeignKey(User, on_delete=models.CASCADE)
    def __str__(self):
        return "Poll:{ " + self.poll.__str__() + " }, Candidate:{ " + self.candidate.__str__() + " }"

class Vote(models.Model):
    voter = models.ForeignKey(User, on_delete=models.CASCADE)
    option = models.ForeignKey(PollOption, on_delete=models.CASCADE)
    def __str__(self):
        return "Voter:{ " + self.voter.__str__() + " }, Option:{ " + self.option.__str__() + " }"

class Authorized(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    def __str__(self):
        return "Poll:{ " + self.poll.__str__() + " }, User:{ " + self.user.__str__() + " }"