# -*- coding: utf-8 -*-

from django.test import TestCase, Client
from django.contrib.auth.models import User
from datetime import timedelta
from django.utils import timezone
from .models import Poll, PollOption

class ActivePollTest(TestCase):
    def test_no_polls(self):
        c = Client()
        response = c.get('/polls/active/')
        self.assertEqual(len(response.context['active_polls_list']), 0)

class EndedPollTest(TestCase):
    def test_no_polls(self):
        c = Client()
        response = c.get('/polls/ended/')
        self.assertEqual(len(response.context['ended_polls_list']), 0)

class IncomingPollTest(TestCase):
    def test_no_polls(self):
        c = Client()
        response = c.get('/polls/incoming/')
        self.assertEqual(len(response.context['incoming_polls_list']), 0)

class VoteViewTest(TestCase):
    def test_no_polls(self):
        c = Client()
        response = c.get('/polls/0/vote/', follow=True)
        self.assertEqual(response.status_code, 404)

    def test_vote_not_started(self):
        c = Client()
        u = User.objects.create_user(username='adam', password='adampass')
        now = timezone.now()
        poll = Poll.objects.create(name='Ankieta', creator=u, vote_start=now+timedelta(days=1),
            vote_end=now+timedelta(days=2), max_vote_number=1)
        PollOption.objects.create(poll=poll, candidate=u)
        response = c.get('/polls/{0}/vote/'.format(poll.id), follow=True)
        self.assertEqual(response.status_code, 404)

    def test_vote_timeout(self):
        c = Client()
        u = User.objects.create_user(username='adam', password='adampass')
        now = timezone.now()
        poll = Poll.objects.create(name='Ankieta', creator=u, vote_start=now-timedelta(days=2),
            vote_end=now-timedelta(days=1), max_vote_number=1)
        PollOption.objects.create(poll=poll, candidate=u)
        response = c.get('/polls/{0}/vote/'.format(poll.id))
        self.assertTrue(response.context['vote_timeout'])

class MyPollsTest(TestCase):
    def test_user_not_logged(self):
        c = Client()
        u = User.objects.create_user(username='adam', password='adampass')
        now = timezone.now()
        poll = Poll.objects.create(name='Ankieta', creator=u, vote_start=now-timedelta(days=2),
            vote_end=now-timedelta(days=1), max_vote_number=1)
        PollOption.objects.create(poll=poll, candidate=u)
        response = c.get('/polls/mypolls/', follow=True)
        self.assertEqual(response.status_code, 404)
        
    def test_no_polls(self):
        c = Client()
        User.objects.create_user(username='adam', password='adampass')
        c.login(username='adam', password='adampass')
        response = c.get('/polls/mypolls/')
        self.assertEqual(len(response.context['my_polls_list']), 0)

class PollOptionTest(TestCase):
    def test_no_polls(self):
        c = Client()
        response = c.get('/polls/0/', follow=True)
        self.assertEqual(response.status_code, 404)

class CreatePollTest(TestCase):
    def test_user_not_logged(self):
        c = Client()
        u = User.objects.create_user(username='adam', password='adampass')
        response = c.get('/polls/create/', follow=True)
        self.assertEqual(response.status_code, 404)

class ChangePollTest(TestCase):
    def test_user_not_logged(self):
        c = Client()
        u = User.objects.create_user(username='adam', password='adampass')
        now = timezone.now()
        poll = Poll.objects.create(name='Ankieta', creator=u, vote_start=now-timedelta(days=2),
            vote_end=now-timedelta(days=1), max_vote_number=1)
        PollOption.objects.create(poll=poll, candidate=u)
        response = c.get('/polls/{0}/change/'.format(poll.id), follow=True)
        self.assertEqual(response.status_code, 404)

    def test_user_not_owner(self):
        c = Client()
        u = User.objects.create_user(username='adam', password='adampass')
        User.objects.create_user(username='adams', password='adampass')
        c.login(username='adams', password='adampass')
        now = timezone.now()
        poll = Poll.objects.create(name='Ankieta', creator=u, vote_start=now-timedelta(days=2),
            vote_end=now-timedelta(days=1), max_vote_number=1)
        PollOption.objects.create(poll=poll, candidate=u)
        response = c.get('/polls/{0}/change/'.format(poll.id), follow=True)
        self.assertEqual(response.status_code, 404)

class PollSummaryTest(TestCase):
    def test_poll_does_not_exists(self):
        c = Client()
        response = c.get('/polls/0/summary/', follow=True)
        self.assertEqual(response.status_code, 404)

    def test_poll_did_not_finish(self):
        c = Client()
        u = User.objects.create_user(username='adam', password='adampass')
        now = timezone.now()
        poll = Poll.objects.create(name='Ankieta', creator=u, vote_start=now-timedelta(days=1),
            vote_end=now+timedelta(days=1), max_vote_number=1)
        PollOption.objects.create(poll=poll, candidate=u)
        response = c.get('/polls/{0}/summary/'.format(poll.id), follow=True)
        self.assertEqual(response.status_code, 404)