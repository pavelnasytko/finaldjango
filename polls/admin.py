from django.contrib import admin

from .models import Poll, PollOption, Vote, Authorized

admin.site.register(Poll)
admin.site.register(PollOption)
admin.site.register(Vote)
admin.site.register(Authorized)
